from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def show_todo_list_list(request):
    todolist_list = TodoList.objects.all()
    context = {
        "todolist_list": todolist_list,
    }
    return render(request, "todos/todoslist.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_object": todolist,
    }
    return render(request, "todos/tododetail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save(False)
            todolist.save()
            return redirect("todo_list_detail", todolist.id)

    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
        return render(request, "todos/createlist.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm(instance=todolist)
        context = {
            "todolist_object": todolist,
            "form": form,
        }
    return render(request, "todos/updatelist.html", context)


def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect(show_todo_list_list)

    return render(request, "todos/deletelist.html")


def todo_list_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            listitem = form.save(False)
            listitem.save()
            return redirect("todo_list_detail", id=listitem.list.id)

    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }
        return render(request, "todos/createitem.html", context)


def todo_list_item_update(request, id):
    listitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=listitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=listitem.list.id)
    else:
        form = TodoItemForm(instance=listitem)
        context = {
            "todolist_object.items": listitem,
            "form": form,
        }
    return render(request, "todos/updateitem.html", context)


# Create your views here.
